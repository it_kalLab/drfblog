from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, verbose_name=_("User"), on_delete=models.CASCADE, related_name="profile")
    birth_date = models.DateField(_("Birth date"), blank=True, null=True)
    address = models.CharField(_("Address"), max_length=50)
    
    class Meta:
        verbose_name = _("UserProfile")
        verbose_name_plural = _("UserProfiles")

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse("UserProfile_detail", kwargs={"pk": self.pk})


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
