from django.contrib.auth.models import User
from .models import (
    UserProfile
)

from rest_framework import serializers



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = [
            'password',
            'last_login',
            'is_superuser',
            'is_staff',
            'is_active',
            'groups',
            'user_permissions',
        ]


class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = [
            "user",
            "birth_date",
            "address",
        ]
