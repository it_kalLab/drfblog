from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework import (
    viewsets, 
    permissions,
    status
)
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view

from django.contrib.auth.models import User
from .serializers import (
    UserSerializer,
    UserProfileSerializer,
    UserProfile
)
from rest_framework import mixins


for user in User.objects.all():
    Token.objects.get_or_create(user=user)


def check_required_fields(fields, post_data):
    not_in_post = []
    error_response = {}

    for field in fields:
        if field not in post_data:
            not_in_post.append(field)

    if len(not_in_post)>0:
        
        error_response["error"] = "Some fields are required"
        error_response["fields"] = not_in_post

        return Response(error_response, status=status.HTTP_400_BAD_REQUEST)


class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


#List, Detail, Update, Delete

class UserProfileView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        profile = request.user.profile
        return Response(UserProfileSerializer(profile).data)



@api_view(['POST'])
def signup(request):
    error_response = {}
    if request.method == "POST":

        required_fields = [
            "username",
            "password",
            "confirm_password",
            "email",
        ]

        checked =  check_required_fields(required_fields, request.data)

        if checked:
            return checked
        

        username = request.data['username']
        password = request.data['password']
        confirm_password = request.data['confirm_password']
        email = request.data['email']

        try:
            user = User.objects.get(username=username)
            if user:
                error_response["error"] = "User with the given username already exist"
                return Response(error_response, status=status.HTTP_400_BAD_REQUEST)
        except:
            pass

        try:
            user = User.objects.get(email=email)
            print(user)
            if user:
                error_response["error"] = "User with the given email already exist"
                return Response(error_response, status=status.HTTP_400_BAD_REQUEST)
        except:
            pass

        
        if password != confirm_password:
            error_response["error"] = "password_error"
            error_response["details"] = "Password and Confirm password not equal"
            return Response(error_response, status=status.HTTP_400_BAD_REQUEST) 

        user = User.objects.create_user(
            username=username,
            password=password,
            email=email
        )
        return Response(UserSerializer(user).data)

@api_view(['POST'])
def signin(request):
    error_response = {}
    if request.method == "POST":
        required_fields = [
            "username",
            "password",
        ]
    
        checked =  check_required_fields(required_fields, request.data)
        
        if checked:
            return checked

        username = request.data['username']
        password = request.data['password']

        user = authenticate(username=username, password=password)

        if not user:
            error_response["error"] = "Authentification error"
            error_response["details"] = "Username or password incorrect"
            return Response(error_response, status=status.HTTP_400_BAD_REQUEST)
        token = Token.objects.get(user=user)

        return Response(
            {"token": token.key}
        )