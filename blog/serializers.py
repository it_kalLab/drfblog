from rest_framework import serializers
from .models import (
    BlogPost,
    BlogComment
)

class BlogCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogComment
        fields = [
            'id',
            'blog',
            'content',
        ]

class BlogPostSerializer(serializers.ModelSerializer):
    #comments = BlogCommentSerializer()

    class Meta:
        model = BlogPost
        fields = [
            'id',
            'title',
            'content',
        ]


