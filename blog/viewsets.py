from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.response import Response
from .serializers import (
    BlogPost,
    BlogPostSerializer,
    BlogComment,
    BlogCommentSerializer,
)

class BlogPostViewset(viewsets.ModelViewSet):
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


    def list(self, request):
        queryset = BlogPost.objects.all()
        serializer = BlogPostSerializer(queryset, many=True)
        data = serializer.data
        list_data = []

        for b in data:
            b_data = dict(b)
            comments = []
            blog = BlogPost.objects.get(id=b["id"])
            for comment in blog.comments.all():
                comment_serialized = BlogCommentSerializer(comment).data
                comments.append(comment_serialized)
                
            b_data["comments"] = comments

            list_data.append(b_data)
        return Response(list_data)


    def create(self, request):
        error_response = {}
        if request.user.is_authenticated:
            blog_post = BlogPost.objects.create(
                title = request.data["title"],
                content=request.data["content"],
                author=request.user
            )
            return Response(BlogPostSerializer(blog_post).data)
        else:
            error_response = {
                "Error": "User needs to be authenticated"
            }
            return Response(error_response)
        return super().create(request)
    


class BlogCommentViewset(viewsets.ModelViewSet):
    queryset = BlogComment.objects.all()
    serializer_class = BlogCommentSerializer

    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


    def create(self, request):
        if request.user.is_authenticated:
            error_response = {}
            blog = None
            try:
                blog=BlogPost.objects.get(pk=request.data['blog'])
            except:
                error_response = {
                    "error": "No BlogPost with the given ID"
                }

            if blog:
                blog_comment = BlogComment.objects.create(
                    blog=blog,
                    content=request.data['content'],
                    author=request.user
                )
                return Response(BlogCommentSerializer(blog_comment).data)
            else:
                return Response(error_response)
        else:
            return super().create(request)
