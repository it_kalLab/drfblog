from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User

# Create your models here.

class BlogPost(models.Model):
    title = models.CharField(_("Title"), max_length=50)
    content = models.TextField(_("Content"))
    author = models.ForeignKey(User, verbose_name=_("Author"), on_delete=models.CASCADE)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _("BlogPost")
        verbose_name_plural = _("BlogPosts")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("BlogPost_detail", kwargs={"pk": self.pk})


class BlogComment(models.Model):
    blog = models.ForeignKey(BlogPost, verbose_name=_("Blog"), on_delete=models.CASCADE, related_name="comments")
    content = models.TextField(_("Content"))

    author = models.ForeignKey(User, verbose_name=_("Author"), on_delete=models.CASCADE)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)


    class Meta:
        verbose_name = _("BlogComment")
        verbose_name_plural = _("BlogComments")

    def __str__(self):
        return self.blog.title

    def get_absolute_url(self):
        return reverse("BlogComment_detail", kwargs={"pk": self.pk})

