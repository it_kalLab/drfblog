from django.contrib import admin
from .models import (
    BlogPost,
    BlogComment
)
# Register your models here.

@admin.register(BlogPost)
class BlogPostAdmin(admin.ModelAdmin):
    pass



@admin.register(BlogComment)
class BlogCommentAdmin(admin.ModelAdmin):
    pass
