from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpRequest
from django.views import generic
from django.core import serializers
from .models import (
    BlogComment,
)
# Create your views here.

class BlogCommentsJsonResponse(generic.View):
    def get(self, request: HttpRequest):
        comments = []
        for comment in BlogComment.objects.all():
            blog_comment = {
                "blog": {
                    "id": comment.blog.id,
                    "title": comment.blog.title,
                    "author": {
                        "username": comment.blog.author.username
                    }
                },
                "content": comment.content,
                "author": comment.author.id
            }

            comments.append(blog_comment)

        data = {
            "comments": comments
        }
        return JsonResponse(data)