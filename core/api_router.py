from rest_framework.routers import DefaultRouter
from blog.viewsets import (
    BlogPostViewset,
    BlogCommentViewset,
)

from account.viewsets import (
    UserViewset,
)

router = DefaultRouter()

router.register('blogs', BlogPostViewset)
router.register("blog-comments", BlogCommentViewset)
router.register("users", UserViewset)
